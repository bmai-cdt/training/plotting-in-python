# plotting-in-python

This tutorial is a simple researcher's guide to plotting in python, written by [Benjamin rhodes](https://benrhodes26.github.io/).

It covers the basic object-oriented mechanics of matplotlib and how to build your own set of useful plotting functions suitable for research. Along the way, we will discuss specific tips and tricks like: why formatting many subplots is surprisingly hard (and how to fix it), how to globally configure matplotlib settings like font sizes, automatic saving of data to enable rapid editing of figures, manual editing of vector graphics and more.

# Files

There are two main files:
* `A researcher's guide to python plotting.ipynb`
* `plotting_utils.py`

The first contains the actual tutorial. A couple of basic functions created in that tutorial are then placed in plotting_utils.py, which you are free to use in your own projects. It is good practice to have such a file of plotting functions that you can re-use.

# Setup
If you wish to create a virtual environment that matches the one I used, then Anaconda** (v 22.9.0) installed, you can simply run: 

`conda env create -f env.yml`

on the command line. This will create a virtual environment called `simple`. After activating it with `conda activate simple`, you can then run `jupyter lab`, which will start up in your browser and allow you to see the contents of the notebook `A researcher's guide to python plotting.ipynb`.


** I actually used [mamba](https://mamba.readthedocs.io/en/latest/installation.html) (v 0.27.0) which is a fast version of conda. I recommend it!
