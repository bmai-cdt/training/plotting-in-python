import os
import matplotlib
from matplotlib import pyplot as plt
from matplotlib.patches import Circle
from matplotlib.patheffects import withStroke

SMALL_SIZE = 8
MEDIUM_SIZE = 10
BIG_SIZE = 12
HUGE_SIZE=14
plt.rc('font', size=MEDIUM_SIZE)          # controls default text sizes
plt.rc('xtick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=MEDIUM_SIZE)    # fontsize of the tick labels
plt.rc('legend', fontsize=MEDIUM_SIZE)    # legend fontsize
plt.rc('axes', titlesize=BIG_SIZE)     # fontsize of the axes title
plt.rc('axes', labelsize=BIG_SIZE)    # fontsize of the x and y labels
plt.rc('figure', titlesize=HUGE_SIZE)  # fontsize of the figure title


def plot(title, xlabel, ylabel, figsize=(6, 4), xscale="linear", yscale="linear", use_grid=True):
    """A super basic plotting function"""
    fig, ax = plt.subplots(figsize=figsize)
    ax.set(xscale=xscale, yscale=yscale, xlabel=xlabel, ylabel=ylabel)  # a convenient way to set lots of properties at once!
    ax.grid(use_grid)
    fig.suptitle(title)  # why not fig.set_title()? I have no idea, and this seems like a bad design choice to me...
    
    return fig, ax


def subplots(nrows, ncols, title, xlabel, ylabel, width=6, height=4, scale="auto", sharex=True, sharey=True, grid=True):
    """A function for making nicely formatted grids of subplots with shared labels on the x-axis and y axis"""
    assert nrows > 1 or ncols > 1, "use plot() to create a single subplot"
    
    fig_width = width * ncols
    fig_height = height * nrows
    
    if scale == "auto":
        scale = min(1.0, 10.0/fig_width)  # width of figure cannot exceede 10 inches
        scale = min(scale, 10.0/fig_height)  # height of figure cannot exceede 10 inches
    
    figsize=(scale * fig_width, scale * fig_height)
    fig, axs = plt.subplots(nrows, ncols, figsize=figsize, sharex=sharex, sharey=sharey)
    
    for ax in axs.flat:
        ax.grid(grid)  # maybe add grid lines
        
    fig.suptitle(title)
    fig.supxlabel(xlabel)  # shared x label
    fig.supylabel(ylabel)  # shared y label
    
    fig.tight_layout()  # adjust the padding between and around subplots to neaten things up
    
    return fig, axs


def save_fig(fig, save_dir, name="", both_formats=True, close=True):
    
    fig.savefig(os.path.join(save_dir, f"{name}.pdf"), bbox_inches='tight', dpi=300)
    
    if both_formats:
        fig.savefig(os.path.join(save_dir, f"{name}.png"), bbox_inches='tight', dpi=300)
    if close:
        plt.close(fig)  # otherwise figure may hang around in memory


def circle_annotate(ax, x, y, text, label):
    """Annotate an axis by drawing a circle at (x,y) and labelling it. 
    Note: (x,y) are specificied in data-coordinates (i.e relative to the scale of the data you have already plotted on ax)
    """
    
    ROYAL_BLUE = [0, 20/256, 82/256]
    
    # Circle marker
    c = Circle((x, y), radius=0.15, clip_on=False, zorder=10, linewidth=1.5,
               edgecolor=ROYAL_BLUE + [0.6], facecolor='none',
               path_effects=[withStroke(linewidth=7, foreground='white')])
    ax.add_artist(c)

    # use path_effects as a background for the texts
    # draw the path_effects and the colored text separately so that the
    # path_effects cannot clip other texts
    for path_effects in [[withStroke(linewidth=7, foreground='white')], []]:
        color = 'white' if path_effects else ROYAL_BLUE
        ax.text(x, y-0.2, text, zorder=100,
                ha='center', va='top', weight='bold', color=color,
                style='italic', fontfamily='Courier New',
                path_effects=path_effects)

        color = 'white' if path_effects else 'black'
        ax.text(x, y-0.33, label, zorder=100,
                ha='center', va='top', weight='normal', color=color,
                fontfamily='monospace', fontsize='medium',
                path_effects=path_effects)
